import Vue from 'vue'
import VueRouter from 'vue-router'
import Aboute from "../components/Aboute.vue";
// import HelloWorld from "../components/HelloWorld"
import Index from "../components/Index"
Vue.use(VueRouter)

const routes = [
  {
path: '/',
name: 'Index',
component: Index
  },
  {
    path: '/about',
    name: 'about',
    component: Aboute
  }
]

// eslint-disable-next-line no-new
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
